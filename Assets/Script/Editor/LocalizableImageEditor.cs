﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.UI;


[CustomEditor(typeof(LocalizableImage))]
public class LocalizableImageEditor : ImageEditor
{
    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        EditorGUILayout.PropertyField(serializedObject.FindProperty("languageToSpriteTupleArray"));
        serializedObject.ApplyModifiedProperties();

        base.OnInspectorGUI();
    }

}