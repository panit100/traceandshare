﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
public class MakeLanguageObject
{
    [MenuItem("Assets/Create/Language Object")]

    public static void Create(){
        LanguageObject asset = ScriptableObject.CreateInstance<LanguageObject>();
        AssetDatabase.CreateAsset(asset,"Assets/NewLanguageObject.asset");
        AssetDatabase.SaveAssets();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;
    }
}
