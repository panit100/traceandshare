﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundSetting : MonoBehaviour
{
    public string SFXkey;
    public string MusicKey;
    [SerializeField] AudioSource SFX;
    [SerializeField] AudioSource Music;

    private void Start() {
        GetSetting();
    }

    void GetSetting(){
        if(PlayerPrefs.GetInt(SFXkey,1) == 1){
            SFX.GetComponent<AudioSource>().mute = false;
        } else{
            SFX.GetComponent<AudioSource>().mute = true;
        }

        if(PlayerPrefs.GetInt(MusicKey,1) == 1){
            Music.GetComponent<AudioSource>().Play();
        } else{
            Music.GetComponent<AudioSource>().Stop();
        }
    }
}
