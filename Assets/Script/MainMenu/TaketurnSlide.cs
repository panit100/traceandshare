﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TaketurnSlide : MonoBehaviour
{
    LineRenderer lineRenderer;
    RaycastHit hit;

    public Transform[] Posiotions;
    float numPoint = 0;
    public float numPoints = 50;

    public GameObject MainObject;

    public int currentPosition = 0;

    float disMainPos;
    float disTouchPos;

    [SerializeField] string nextScene;
    [SerializeField] SaveTaketurn saveTaketurn = null;

    void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();       
    }

    void Start(){
        lineRenderer.positionCount = Convert.ToInt32(numPoints);

        CrateLine(0,Posiotions[0],Posiotions[1]);

        MainObject.transform.position = lineRenderer.GetPosition(currentPosition);
        disMainPos = Vector3.Distance(MainObject.transform.position,lineRenderer.GetPosition(lineRenderer.positionCount-1));
        
    }

    void CrateLine(int n,Transform p0,Transform p1){
        
        numPoint += numPoints;

        for(int i = 1; i < numPoints+1; i++){
            float T = i/numPoints;
            Vector3 pos = CalculateLinearPoint(T,p0.position,p1.position);
            pos.z += MainObject.transform.position.z;
            lineRenderer.SetPosition(n,pos);
            n++;
        }
    }

    Vector3 CalculateLinearPoint(float t,Vector3 p0,Vector3 p1){
        return p0 + t * (p1 - p0);
    }

    private void OnMouseDrag() {
        
        currentPosition += 1;

        if(currentPosition >= lineRenderer.positionCount-1){
            currentPosition = lineRenderer.positionCount - 1;
        }
        Vector3 current = new Vector3(lineRenderer.GetPosition(currentPosition).x,lineRenderer.GetPosition(currentPosition).y,lineRenderer.GetPosition(currentPosition).z);
        transform.position = current;
                
        for(int i = 0; i < currentPosition; i++){
            lineRenderer.SetPosition(i,MainObject.transform.position);
        }

        CheckEndLine();
    }

    void CheckEndLine(){

        if(transform.position.x == lineRenderer.GetPosition(lineRenderer.positionCount-1).x && transform.position.y == lineRenderer.GetPosition(lineRenderer.positionCount-1).y){
            saveTaketurn.UpdateData();
            SceneManager.LoadScene(nextScene);
        }
    }
}
