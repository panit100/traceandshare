﻿public interface ILocalizable
{
    void SetLanguage( Language language );
}
