﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class SetLanguageButton : MonoBehaviour
{
    
    public Toggle toggle;
    public int LGNum;
    public void SelectLanguage(int languageIndex)
    {
        Language language = (Language)languageIndex;
        Debug.Log((Language)languageIndex);
        
        LanguageManager.SetSelectedLanguage(language);


    }
    private void Start()
    {
        toggle.onValueChanged.AddListener((value) =>
        {
            MyListener(value);
        });

    }
    public void MyListener(bool value)
    {
        if (value)
        {
            SelectLanguage(LGNum);
        }
        else
        {
            
        }

    }

}    