﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;


public class LocalizableImage : Image, ILocalizable 
{
    [Serializable]
    public struct LanguageToSpriteTuple
    {
        public Language language;
        public Sprite sprite;
    }

    [SerializeField]
    private List<LanguageToSpriteTuple> languageToSpriteTupleArray;
    


    public void SetLanguage(Language language)
    {
        

        Sprite targetSprite = null;

        foreach (LanguageToSpriteTuple languageToSpriteTuple in languageToSpriteTupleArray)
        {
           
            if (languageToSpriteTuple.language == language)
            {
                targetSprite = languageToSpriteTuple.sprite;
            }
        }

        if (targetSprite == null)
        {
            throw new Exception("LocalizableImage::SetLanguage() - Given language not found.");
        }
        
        if(this != null)
        this.sprite = targetSprite;

    }

    void Start()
    {
        LanguageManager.RegisterILocalizable(this);
        SetLanguage(LanguageManager.GetSelectedLanguage());

    }
}

