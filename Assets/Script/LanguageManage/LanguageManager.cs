﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Language
{
    TH,EN,CH
}

public class LanguageManager
{
    public static LanguageManager instance = new LanguageManager();

    private Language selectedLanguage = Language.TH;

    private List<ILocalizable> iLocalizableList = new List<ILocalizable>();

    public static void RegisterILocalizable( ILocalizable ilocalizable )
    {
        instance.iLocalizableList.Add(ilocalizable);
    }
    public static void ClearEmptyLocalizable(){
        instance.iLocalizableList.RemoveAll(o => o == null);
    }
   
    public static Language GetSelectedLanguage()
    {
        return instance.selectedLanguage;
    }

    public static void SetSelectedLanguage( Language language )
    {

        instance.selectedLanguage = language;
        foreach (ILocalizable ilocalizable in instance.iLocalizableList)
        {
            ilocalizable.SetLanguage(instance.selectedLanguage);
            
        }
    }
    
}
