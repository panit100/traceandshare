﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoManager : MonoBehaviour
{
    VideoPlayer video;
    public bool PlayOnAwake = false;
    bool alreadyPlay = false;
    void Start()
    {
        video = GetComponent<VideoPlayer>();
        video.Prepare();
    }

    void Update()
    {
        if(alreadyPlay){
            return;
        }
        if(video.isPrepared && PlayOnAwake){
            video.Play();
            alreadyPlay = true;
        }
    }


}
