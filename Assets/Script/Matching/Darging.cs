﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Darging : MonoBehaviour
{
    [SerializeField]
    Vector2 initialPosition;
    float deltaX, deltaY;
    Ray ray;
    RaycastHit raycastHit;

    
    void Start()
    {
        initialPosition = transform.position;      
    }

    void Update()
    {
        //MoveObjectWithMouse();
        MoveObjectWithTouch();
    }

    void MoveObjectWithTouch(){
        if(Input.touchCount > 0){
            Touch touch = Input.GetTouch(0);
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(touch.position);
            ray = Camera.main.ScreenPointToRay(touch.position);

            switch(touch.phase){
                case TouchPhase.Began:
                    if(GetComponent<Collider2D>() == Physics2D.OverlapPoint(touchPos)){
                    deltaX = touchPos.x - transform.position.x;
                    deltaY = touchPos.y - transform.position.y;
                    }
                    break;
                
                case TouchPhase.Moved:
                    if(GetComponent<Collider2D>() == Physics2D.OverlapPoint(touchPos)){
                    transform.position = new Vector2(touchPos.x - deltaX,touchPos.y - deltaY);
                }
                    break;
                
                case TouchPhase.Ended:
                        if(GetComponent<Collider2D>() == Physics2D.OverlapPoint(touchPos)){
                    if(Physics.Raycast(ray,out raycastHit)){
                        
                        if(raycastHit.collider.gameObject.tag == gameObject.tag){
                            
                            raycastHit.collider.gameObject.GetComponent<SpriteRenderer>().enabled = true;
                        }
            
                    }
                }
                
                transform.position = new Vector2(initialPosition.x,initialPosition.y);
                    break;
            }
        }
    }

    void MoveObjectWithMouse(){
            Vector2 MousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            

            if(Input.GetMouseButtonDown(0)){
                if(GetComponent<Collider2D>() == Physics2D.OverlapPoint(MousePos)){
                    deltaX = MousePos.x - transform.position.x;
                    deltaY = MousePos.y - transform.position.y;
                }
            }

            if(Input.GetMouseButton(0)){    

                if(GetComponent<Collider2D>() == Physics2D.OverlapPoint(MousePos)){
                    transform.position = new Vector2(MousePos.x - deltaX,MousePos.y - deltaY);
                }
            }

            if(Input.GetMouseButtonUp(0)){
                
                if(GetComponent<Collider2D>() == Physics2D.OverlapPoint(MousePos)){
                    if(Physics.Raycast(ray,out raycastHit)){
                        
                        if(raycastHit.collider.gameObject.tag == gameObject.tag){
                            
                            raycastHit.collider.gameObject.GetComponent<SpriteRenderer>().enabled = true;
                        }
            
                    }
                }
                
                transform.position = new Vector2(initialPosition.x,initialPosition.y);
            }
    }   

    
}
