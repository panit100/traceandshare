﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class CheckColor : MonoBehaviour
{
    [SerializeField]
    SpriteRenderer[] sprites;
    
    [SerializeField]
    string nextScene = null;
    [SerializeField]
    GameObject Color = null;
    [SerializeField]
    GameObject BoardColor = null;
    [SerializeField]
    GameObject BackButton = null;
    [SerializeField]
    GameObject VideoPlay = null;
    [SerializeField]
    VideoPlayer video = null;
    [SerializeField] SaveData saveData;
    bool isPlay = false;
    [SerializeField] bool isTimeStamp = false;
    bool isCheck;

    void Start()
    {
        if(isTimeStamp){
            saveData.StartTimeStamp();
        }        
    }

    void Update(){
        if(!isCheck){
            CheckSprite();
            }
    }

    void CheckSprite(){
        foreach(SpriteRenderer n in sprites){
            if(n.color.a < 1){
                return;
            }
        }
        saveData.Activate();

        Color.SetActive(false);

        if(BoardColor != null)
        BoardColor.SetActive(false);
        
        VideoPlay.SetActive(true);

        if(BackButton != null)
        BackButton.SetActive(true);

        if(video.isPlaying == false && !isPlay){
            video.Play();
            isPlay = true;
        }

        if(BackButton == null){
            video.loopPointReached += NextScene;  
        }

        isCheck = true;        
    }

    void NextScene(VideoPlayer vp){
        SceneManager.LoadScene(nextScene);
    }

}
