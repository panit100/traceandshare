﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckDraw : MonoBehaviour
{
    [SerializeField] string Key = null;
    [SerializeField] GameObject ButtonBFDraw = null;
    [SerializeField] GameObject ButtonAFDraw = null;

    void Start()
    {
        if(ButtonBFDraw != null){
            CheckIsDraw();
        }        
    }

    public void SaveIsDraw(){
        PlayerPrefs.SetInt(Key,1);
    }

    void  CheckIsDraw(){
        if(PlayerPrefs.GetInt(Key) == 1){
            ButtonBFDraw.SetActive(false);
            ButtonAFDraw.SetActive(true);
        }
    }

    public void ResetIsDraw(){
        PlayerPrefs.DeleteKey("AlarmClock");
        PlayerPrefs.DeleteKey("Ball");
        PlayerPrefs.DeleteKey("Fan");
    }
}
