﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;
using System;

public class ScrubbableSpriteForMatching : MonoBehaviour
{
    public Texture2D textureBrush;
    public int textureBrushPivotX;
    public int textureBrushPivotY;

    public SpriteMask spriteMask;
    public SpriteRenderer dashSpriteRenderer;
    public SpriteRenderer fillSpriteRenderer;

    public int acceptableErrorPercentage;
    private double acceptableError;

    public ComputeShader computeShader;
    private int OutMaskAbsDiffKernelId;

    private Color[] maskColorArray;

    private RenderTexture resTexture;
    private Color[] errorColorArray;

    private Texture2D tmpTexture;

    Sprite GetEmptySprite( Sprite srcSprite ){
        Texture2D tmpTexture = new Texture2D( srcSprite.texture.width, srcSprite.texture.height );
        Color[] emptyColorArray = new Color[ srcSprite.texture.width * srcSprite.texture.height ];
        tmpTexture.SetPixels(emptyColorArray);
        tmpTexture.Apply();
        Sprite tmpSprite = Sprite.Create( tmpTexture,
                                        new Rect (0, 0, tmpTexture.width, tmpTexture.height),
                                        new Vector2 (0.5f, 0.5f),
                                        srcSprite.pixelsPerUnit);

        return tmpSprite;
    }

    public void Paint( int centerX, int centerY, Color c, int texturePivotX, int texturePivotY, Texture2D textureBrush ){
        for(int i=0; i<textureBrush.width; i++){
            for(int j=0; j<textureBrush.height; j++){

                int paintingX = (int)Mathf.Clamp( centerX - texturePivotX + i, 0, spriteMask.sprite.texture.width-1 );
                int paintingY = (int)Mathf.Clamp( centerY - texturePivotY + j, 0, spriteMask.sprite.texture.height-1 );

                //Color originalColor = maskColorArray[ paintingX + paintingY * spriteMask.sprite.texture.width ];
                //float blendFactor = textureBrush.GetPixel( i, j ).a;
                //Color paintingColor = Color.Lerp( originalColor, c, blendFactor );

                //maskColorArray[ paintingX + paintingY * spriteMask.sprite.texture.width ] = paintingColor;

                //  NOTE - This is an optimized version
                maskColorArray[paintingX + paintingY * spriteMask.sprite.texture.width] = c;
            }
        }

        spriteMask.sprite.texture.SetPixels(maskColorArray);
        spriteMask.sprite.texture.Apply();
    }

    public bool IsAllPainted()
    {
        computeShader.Dispatch(OutMaskAbsDiffKernelId, resTexture.width / 8, resTexture.height / 8, 1);

        errorColorArray = GetPixelsFromRenderTexture();

        double acc = 0;
        for (int i = 0; i < errorColorArray.Length; i++)
        {
            acc += new Vector3(errorColorArray[i].r, errorColorArray[i].g, errorColorArray[i].b).magnitude;
        }

        return acc <= acceptableError;
    }

    Color[] GetPixelsFromRenderTexture()
    {

        RenderTexture previousRenderTexture = RenderTexture.active;

        RenderTexture renderTexture = new RenderTexture(resTexture.width, resTexture.height, 32);
        Graphics.Blit(resTexture, renderTexture);

        RenderTexture.active = renderTexture;
        tmpTexture.ReadPixels(new Rect(0, 0, resTexture.width, resTexture.height), 0, 0);
        tmpTexture.Apply();

        RenderTexture.active = previousRenderTexture;

        return tmpTexture.GetPixels();
    }

    void Start(){

        Sprite maskSprite = GetEmptySprite( dashSpriteRenderer.sprite );
        spriteMask.sprite = maskSprite;

        OutMaskAbsDiffKernelId = computeShader.FindKernel("OutMaskAbsDiff");

        resTexture = new RenderTexture( dashSpriteRenderer.sprite.texture.width,
                                        dashSpriteRenderer.sprite.texture.height,
                                        32,
                                        RenderTextureFormat.ARGB32 );
        resTexture.enableRandomWrite = true;
        resTexture.Create();

        maskColorArray = spriteMask.sprite.texture.GetPixels();

        computeShader.SetTexture(OutMaskAbsDiffKernelId, "srcTexture", dashSpriteRenderer.sprite.texture);
        computeShader.SetTexture(OutMaskAbsDiffKernelId, "dstTexture", fillSpriteRenderer.sprite.texture);
        computeShader.SetTexture(OutMaskAbsDiffKernelId, "maskTexture", spriteMask.sprite.texture);
        computeShader.SetTexture(OutMaskAbsDiffKernelId, "resTexture", resTexture);
        computeShader.Dispatch(OutMaskAbsDiffKernelId, resTexture.width / 8, resTexture.height / 8, 1);

        tmpTexture = new Texture2D(dashSpriteRenderer.sprite.texture.width, dashSpriteRenderer.sprite.texture.height, TextureFormat.ARGB32, false);

        errorColorArray = GetPixelsFromRenderTexture();
        double acc = 0;
        for (int i = 0; i < errorColorArray.Length; i++)
        {
            acc += new Vector3(errorColorArray[i].r, errorColorArray[i].g, errorColorArray[i].b).magnitude;
        }
        acceptableError = acc * ( acceptableErrorPercentage / 100.0 );

        Debug.Log(string.Format("acceptableError = {0}", acceptableError));
    }

    void Update(){
        DrawWithMouse();
        //DrawWithTouch();

        

        //  NOTE - For debugging
        //if (Input.GetKeyDown(KeyCode.A))
        //{
        //    computeShader.Dispatch(OutMaskAbsDiffKernelId, resTexture.width / 8, resTexture.height / 8, 1);

        //    errorColorArray = GetPixelsFromRenderTexture();

        //    double acc = 0;
        //    for (int i = 0; i < errorColorArray.Length; i++)
        //    {
        //        acc += new Vector3( errorColorArray[i].r, errorColorArray[i].g, errorColorArray[i].b ).magnitude;
        //    }
        //    Debug.Log(acc);
        //}
    }
    [SerializeField] string nextScene;
    void DrawWithMouse(){
        if(Input.GetMouseButton(0))
        {
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePos += (Vector2)spriteMask.sprite.bounds.extents;
            mousePos *= spriteMask.sprite.pixelsPerUnit;
            
            //StartCoroutine(CheckPainted());

            Paint((int)mousePos.x, (int)mousePos.y, Color.green, textureBrushPivotX, textureBrushPivotY, textureBrush);
        }
    }

    void DrawWithTouch(){
        if(Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(touch.position);
            touchPos += (Vector2)spriteMask.sprite.bounds.extents;
            touchPos *= spriteMask.sprite.pixelsPerUnit;

            if(touch.phase == TouchPhase.Moved){
                //StartCoroutine(CheckPainted());

                Paint((int)touchPos.x, (int)touchPos.y, Color.green, textureBrushPivotX, textureBrushPivotY, textureBrush);
            }
        }
    }

    IEnumerator CheckPainted(){
        yield return new WaitForSeconds(20f);
        if (IsAllPainted()){
            Debug.Log("Paint done!");
            SceneManager.LoadScene(nextScene);
        }
    }
    
}
