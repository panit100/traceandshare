﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paint : MonoBehaviour
{
    Ray ray;
    RaycastHit hit;
    public Color color;
    Color currentColor;
    float a = 0;
    


    void Update()
    {
        currentColor = color;
        currentColor.a = a;
        PaintWithMouse();
        //PaintWithTouch();
    }

    void PaintWithMouse(){

        ray = Camera.main.ScreenPointToRay(Input.mousePosition);


        if(Input.GetMouseButton(0)){    
            if(Physics.Raycast(ray,out hit)){
                if(hit.collider.gameObject.tag == "isPaint"){
                        if(hit.collider.gameObject == gameObject){
                            if(hit.collider.GetComponent<SpriteRenderer>().color.a >= 1f){
                                return;
                            }
                        a += 1f * Time.deltaTime;
                        hit.collider.GetComponent<SpriteRenderer>().color = currentColor;
                    }
                }
            }
        }
    }

    void PaintWithTouch(){
        if(Input.touchCount > 0){
            Touch touch = Input.GetTouch(0);
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(touch.position);
            ray = Camera.main.ScreenPointToRay(touch.position);

            if(touch.phase == TouchPhase.Moved){    
                if(Physics.Raycast(ray,out hit)){
                    if(hit.collider.gameObject.tag == "isPaint"){
                            if(hit.collider.gameObject == gameObject){
                                if(hit.collider.GetComponent<SpriteRenderer>().color.a >= 1f){
                                    return;
                                }
                            a += 1f * Time.deltaTime;
                            hit.collider.GetComponent<SpriteRenderer>().color = currentColor;
                        }
                    }
                }
            }
        }
    }
}
