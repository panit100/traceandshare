﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class CheckColor_Matching : MonoBehaviour
{
    [SerializeField]
    SpriteRenderer[] sprites;
    
    [SerializeField]
    string nextScene = null;

    void Update(){
        CheckSprite();
    }

    void CheckSprite(){
        foreach(SpriteRenderer n in sprites){
            if(n.color.a < 1){
                return;
            }
        }

        SceneManager.LoadScene(nextScene);
        }        
}

