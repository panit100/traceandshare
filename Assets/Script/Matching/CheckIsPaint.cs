﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class CheckIsPaint : MonoBehaviour
{
    [SerializeField] GameObject[] AFColor;
    [SerializeField] GameObject videoPlayer;
    [SerializeField] VideoPlayer video;
    bool isPlay = false;

    void Update()
    {
        CheckAlreadyPaint();        
    }

    void CheckAlreadyPaint(){
        foreach(GameObject n in AFColor){
            if(n.active == false){
                return;
            }
        }

        videoPlayer.SetActive(true);
        if(!video.isPlaying && !isPlay){
            video.Play();
            isPlay = true;
            video.loopPointReached += isVideoEnd; 
        }

         
    }

    void isVideoEnd(VideoPlayer vp){
        SceneManager.LoadScene("Mainmenu");
    }
}
