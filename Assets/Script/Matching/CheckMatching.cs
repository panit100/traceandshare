﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CheckMatching : MonoBehaviour
{
    [SerializeField]
    SpriteRenderer[] sprites;
    
    [SerializeField]
    string nextScene;

    [SerializeField] SaveData saveData;

    private void Update() {
        CheckEnableSprite();
    }

    void CheckEnableSprite(){
        foreach(SpriteRenderer n in sprites){
            if(n.enabled != true){
                return;
            }
        }
        saveData.Activate();
        
        SceneManager.LoadScene(nextScene);

    }
}
