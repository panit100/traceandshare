﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class CountVideo : MonoBehaviour
{
    [SerializeField] VideoPlayer video;
    public string NextScene;

    void Start()
    {
        video.loopPointReached += CheckOver;     
    }

    void CheckOver(VideoPlayer vp){
            SceneManager.LoadScene(NextScene);
    }
}
