﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayEndVideo : MonoBehaviour
{
    [SerializeField] VideoPlayer WinVideo = null;
    //[SerializeField] AudioSource WinAudio;
    [SerializeField] GameObject Line;
    [SerializeField] GameObject MainObject = null;
    [SerializeField] Image Background;

    [SerializeField] AudioSource WinkSound;

    [SerializeField] GameObject[] DeactiveObj;
    
    [SerializeField] SaveData saveData = null;
    bool isPlayWink = false;

    public string NextScene;

    public void PlayWink(){
        if(!isPlayWink){
            isPlayWink = true;

            if(DeactiveObj != null){
                foreach(GameObject n in DeactiveObj){
                    n.SetActive(false);
               }
            }

            Background.color = new Color(Background.color.r,Background.color.g,Background.color.b,1);
            
            WinkSound.Play();
            if(saveData != null){
                saveData.Activate();
                }
        }

        Invoke("EndVideo",WinkSound.clip.length);
    }


    public void EndVideo(){
        //Line.GetComponent<LineRenderer>().enabled = false;
        MainObject.GetComponent<SpriteRenderer>().enabled = false;

        if(!WinVideo.isPlaying){
            WinkSound.Stop();

            WinVideo.GetComponent<VideoPlayer>().Play();
        }
         
            WinVideo.loopPointReached += LoadScene;
        
    }

    [SerializeField] bool Stage3;
    public string TaketurnScene = null;
    
    void LoadScene(VideoPlayer vp){
        if(PlayerPrefs.GetInt("Sharing",1) == 1 && Stage3){
            SceneManager.LoadScene(TaketurnScene);
        } else SceneManager.LoadScene(NextScene);
    }
}


