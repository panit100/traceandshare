﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeBG : MonoBehaviour
{
    Image Background;
    float count = 0;
    float fade = 1;
    bool isFade = true;

    private void Start() {
        Background = GetComponent<Image>();
    }
    void Update()
    {
        if(isFade){
            FadeBackground();
        }
    }

    //animation
    void FadeBackground(){

        count += Time.deltaTime;
        
        if(count >= .1f){
            fade -= .1f;
            Background.color = new Color(Background.color.r,Background.color.g,Background.color.b,fade);
            count = 0;
    
        }

        if(Background.color.a <= 0){
            isFade = false;
        }
    }
}
