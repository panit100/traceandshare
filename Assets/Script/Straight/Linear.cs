﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class Linear : MonoBehaviour
{
    LineRenderer lineRenderer;
    RaycastHit hit;

    public PlayEndVideo playEndVideo;

    public Transform[] Posiotions;
    int IndexPositions = 0;
    float numPoint = 0;
    public float numPoints = 50;

    public GameObject MainObject;
    public GameObject CurrentLine;
    public GameObject NextLine;
    int currentPosition = 0;

    float disMainPos;
    float disTouchPos;

    bool isPlay = false;

    void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();       
    }

    void Start(){
        lineRenderer.positionCount = Convert.ToInt32(numPoints);

        CrateLine(0,Posiotions[0],Posiotions[1]);

        MainObject.transform.position = lineRenderer.GetPosition(currentPosition);
        disMainPos = Vector3.Distance(MainObject.transform.position,lineRenderer.GetPosition(lineRenderer.positionCount-1));
        
    }

    private void Update() {
        
            CurrentPos();

            CheckEndLine();
        
    }

    void CrateLine(int n,Transform p0,Transform p1){
        
        numPoint += numPoints;

        for(int i = 1; i < numPoints+1; i++){
            float T = i/numPoints;
            Vector3 pos = CalculateLinearPoint(T,p0.position,p1.position);
            pos.z += MainObject.transform.position.z;
            lineRenderer.SetPosition(n,pos);
            n++;
        }

        IndexPositions++;

        if(IndexPositions < Posiotions.Length-1){
            lineRenderer.positionCount += Convert.ToInt32(numPoints);
            CrateLine(Convert.ToInt32(numPoint),Posiotions[IndexPositions],Posiotions[IndexPositions+1]);
        }else return;
    
    }

    Vector3 CalculateLinearPoint(float t,Vector3 p0,Vector3 p1){
        return p0 + t * (p1 - p0);
    }

    void CurrentPos(){
        
        
        if(Input.touchCount > 0){
            Touch touch = Input.GetTouch(0);

            Vector3 TouchPos = touch.position;
            TouchPos.z = 0;

            disTouchPos = Vector3.Distance(TouchPos,lineRenderer.GetPosition(lineRenderer.positionCount-1));

            Ray ray = Camera.main.ScreenPointToRay(touch.position);

            //print(TouchPos);      
            if(Physics.Raycast(ray,out hit) &&  disTouchPos > disMainPos && touch.phase == TouchPhase.Moved){
                currentPosition += 1;

                if(currentPosition >= lineRenderer.positionCount-1){
                    currentPosition = lineRenderer.positionCount - 1;
                }
                Vector3 current = new Vector3(lineRenderer.GetPosition(currentPosition).x,lineRenderer.GetPosition(currentPosition).y,lineRenderer.GetPosition(currentPosition).z);
                MainObject.transform.position = current;
                
            
                for(int i = 0; i < currentPosition; i++){
                    lineRenderer.SetPosition(i,MainObject.transform.position);
                }

                MainObject.GetComponent<Animator>().SetBool("isPlay",true);
                if(MainObject.GetComponent<AudioSource>().isPlaying){
                    return;
                }
                MainObject.GetComponent<AudioSource>().Play();
            }
        }else{
            MainObject.GetComponent<Animator>().SetBool("isPlay",false);
            MainObject.GetComponent<AudioSource>().Stop();
        }
    }

    void CheckEndLine(){

        if(MainObject.transform.position.x == lineRenderer.GetPosition(lineRenderer.positionCount-1).x && MainObject.transform.position.y == lineRenderer.GetPosition(lineRenderer.positionCount-1).y){
            if(NextLine != null){
                MainObject.GetComponent<Animator>().SetBool("isEnd",true);
                if(!isPlay){
                    WinkSound.Play();
                    isPlay = true;
                    }
                Invoke("ActiveNextline",WinkSound.clip.length);
                return;
            }
            MainObject.GetComponent<Animator>().SetBool("isEnd",true);
        }
    }

    [SerializeField] AudioSource WinkSound = null;
    void ActiveNextline(){
        CurrentLine.SetActive(false);
        NextLine.SetActive(true);
    }

    

}
