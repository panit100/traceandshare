﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;


public class SineFunctionSampler : AbstractFunctionSampler
{
	public float amplitude, frequency, phase;
	LineRenderer lineRenderer;
	RaycastHit hit;
    public PlayEndVideo playEndVideo;

    public GameObject MainObject;
    public GameObject CurrentLine;
    public GameObject NextLine;
    int currentPosition = 0;

    float disMainPos;
    float disTouchPos;

	public SineFunctionSampler( float amplitudeValue, float frequencyValue, float phaseValue ) : base()
	{
		amplitude = amplitudeValue;
		frequency = frequencyValue;
		phase = phaseValue;
	}

	public override float GetValue( float parameter )
	{
		if( parameter < domainMin || parameter > domainMax )
			throw new System.Exception("SineFunctionSampler::GetValue() - Given parameter is out of the function domain.");
		return amplitude * Mathf.Sin( frequency*parameter + phase );
	}

	public void Draw(){
		if(lineRenderer != null)
		{
			for(int i = 0;i < displaySampleCount;i++){
				Vector2 linePos = new Vector2(GetSamplePointList()[i].x+gameObject.transform.position.x,GetSamplePointList()[i].y+gameObject.transform.position.y);
				lineRenderer.SetPosition(i,linePos);
			}
		}
	}

	void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();       
    }

	private void Start() {
		lineRenderer.positionCount = Convert.ToInt32(displaySampleCount);

		Draw();

		MainObject.transform.position = lineRenderer.GetPosition(currentPosition);
		disMainPos = Vector3.Distance(MainObject.transform.position,lineRenderer.GetPosition(lineRenderer.positionCount-1));
	}

	private void Update() {
		//Draw();

		CurrentPos();

        CheckEndLine();
	}

	void CurrentPos(){
        
        
        if(Input.touchCount > 0){
            Touch touch = Input.GetTouch(0);

            Vector3 TouchPos = touch.position;
            TouchPos.z = 0;

            disTouchPos = Vector3.Distance(TouchPos,lineRenderer.GetPosition(lineRenderer.positionCount-1));

            Ray ray = Camera.main.ScreenPointToRay(touch.position);

            //print(TouchPos);      
            if(Physics.Raycast(ray,out hit) &&  disTouchPos > disMainPos && touch.phase == TouchPhase.Moved){
                currentPosition += 1;

                if(currentPosition >= lineRenderer.positionCount-1){
                    currentPosition = lineRenderer.positionCount - 1;
                }
                Vector3 current = new Vector3(lineRenderer.GetPosition(currentPosition).x,lineRenderer.GetPosition(currentPosition).y,0);

                MainObject.transform.position = current;
                
            
                for(int i = 0; i < currentPosition; i++){
                    lineRenderer.SetPosition(i,MainObject.transform.position);
                }

                MainObject.GetComponent<Animator>().SetBool("isPlay",true);
                if(MainObject.GetComponent<AudioSource>().isPlaying){
                    return;
                }
                MainObject.GetComponent<AudioSource>().Play();
            }
        }else{
            MainObject.GetComponent<Animator>().SetBool("isPlay",false);
            MainObject.GetComponent<AudioSource>().Stop();
        }
    }

    void CheckEndLine(){

        if(MainObject.transform.position.x == lineRenderer.GetPosition(lineRenderer.positionCount-1).x && MainObject.transform.position.y == lineRenderer.GetPosition(lineRenderer.positionCount-1).y){
            if(NextLine != null){
                MainObject.GetComponent<Animator>().SetBool("isEnd",true);
                if(!WinkSound.isPlaying){
                    WinkSound.Play();
                    }
                Invoke("ActiveNextline",WinkSound.clip.length);
                return;
            }
            MainObject.GetComponent<Animator>().SetBool("isEnd",true);
        }
    }

    [SerializeField] AudioSource WinkSound;
    void ActiveNextline(){
        CurrentLine.SetActive(false);
        NextLine.SetActive(true);
    }
}