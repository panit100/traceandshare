﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class ScriptTester : MonoBehaviour
{
    public List<Transform> controlPointTransformList;

    public int step;

    [SerializeField]
    private LineRenderer arcLineRenderer;

    [SerializeField]
    private LineRenderer circleLineRenderer;

    LineRenderer lineRenderer;
    RaycastHit hit;
    public PlayEndVideo playEndVideo;

    public GameObject MainObject;
    public GameObject CurrentLine;
    public GameObject NextLine;
    int currentPosition = 0;

    float disMainPos;
    float disTouchPos;


    void Draw()
    {
        if (controlPointTransformList.Count < 3)
            return;

        Vector2[] points = { controlPointTransformList[0].position,
                            controlPointTransformList[1].position,
                            controlPointTransformList[2].position };

        float[] angles = Triangle.GetAngles(points[0], points[1], points[2]);

        int maxAngleIndex = 0;
        for (int i = 1; i < 3; i++)
        {
            if (angles[i] > angles[maxAngleIndex])
                maxAngleIndex = i;
        }

        Vector2 midPoint = points[maxAngleIndex];
        Vector2 startPoint = points[(maxAngleIndex + 1) % 3];
        Vector2 endPoint = points[(maxAngleIndex + 2) % 3];

        Circle c = new Circle(startPoint, midPoint, endPoint);

        float beginAngle = c.GetAngle(startPoint);
        float endAngle = c.GetAngle(endPoint);
        float stepAngle = (endAngle - beginAngle) / (step-1);

        if(arcLineRenderer != null){
            for (int i = 0; i < step; i++)
            {
                arcLineRenderer.SetPosition(i, c.GetPoint(beginAngle + i * stepAngle));
            }
        }

        beginAngle = 0;
        endAngle = 2*Mathf.PI;
        stepAngle = (endAngle - beginAngle) / (2*step - 1);

        if(circleLineRenderer != null){
            for (int i = 0; i < 2*step; i++)
            {
                circleLineRenderer.SetPosition(i, c.GetPoint(beginAngle + i * stepAngle));
            }
        }
    }

    void CurrentPos(){
        if(Input.touchCount > 0){
            Touch touch = Input.GetTouch(0);

            Vector3 TouchPos = touch.position;
            TouchPos.z = 0;

            disTouchPos = Vector3.Distance(TouchPos,lineRenderer.GetPosition(lineRenderer.positionCount-1));

            Ray ray = Camera.main.ScreenPointToRay(touch.position);

            //print(TouchPos);      
            if(Physics.Raycast(ray,out hit) &&  disTouchPos > disMainPos && touch.phase == TouchPhase.Moved){
                currentPosition += 1;

                if(currentPosition >= lineRenderer.positionCount-1){
                    currentPosition = lineRenderer.positionCount - 1;
                }
                Vector3 current = new Vector3(lineRenderer.GetPosition(currentPosition).x,lineRenderer.GetPosition(currentPosition).y,0);

                MainObject.transform.position = current;
                
            
                for(int i = 0; i < currentPosition; i++){
                    lineRenderer.SetPosition(i,MainObject.transform.position);
                }

                MainObject.GetComponent<Animator>().SetBool("isPlay",true);
                if(MainObject.GetComponent<AudioSource>().isPlaying){
                    return;
                }
                MainObject.GetComponent<AudioSource>().Play();
            }
        }else{
            MainObject.GetComponent<Animator>().SetBool("isPlay",false);
            MainObject.GetComponent<AudioSource>().Stop();
        }
    }

    void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();       
    }

    private void Start()
    {
        if (controlPointTransformList.Count < 3)
            return;

        if(arcLineRenderer!= null){
            arcLineRenderer.SetVertexCount(step);
        }


        if(circleLineRenderer != null){
            circleLineRenderer.SetVertexCount(2*step);
        }
        Draw();

        MainObject.transform.position = lineRenderer.GetPosition(currentPosition);
        disMainPos = Vector3.Distance(MainObject.transform.position,lineRenderer.GetPosition(lineRenderer.positionCount-1));
    }

    private void Update()
    {
        //Draw();

        CurrentPos();

        CheckEndLine();
    }

    void CheckEndLine(){

        if(MainObject.transform.position.x == lineRenderer.GetPosition(lineRenderer.positionCount-1).x && MainObject.transform.position.y == lineRenderer.GetPosition(lineRenderer.positionCount-1).y){
            if(NextLine != null){
                MainObject.GetComponent<Animator>().SetBool("isEnd",true);
                if(!WinkSound.isPlaying){
                    WinkSound.Play();
                    }
                Invoke("ActiveNextline",WinkSound.clip.length);
                return;
            }
            MainObject.GetComponent<Animator>().SetBool("isEnd",true);
        }
    }

    [SerializeField] AudioSource WinkSound;
    void ActiveNextline(){
        CurrentLine.SetActive(false);
        NextLine.SetActive(true);
    }
}
