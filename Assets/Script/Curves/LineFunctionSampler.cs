﻿using System.Collections.Generic;
using UnityEngine;

public class LineFunctionSampler : AbstractFunctionSampler
{
	public float slope;

	public LineFunctionSampler( float slopeValue ) : base()
	{
		slope = slopeValue;
	}

	public override float GetValue( float parameter )
	{
		if( parameter < domainMin || parameter > domainMax )
			throw new System.Exception("LineFunctionSampler::GetValue() - Given parameter is out of the function domain.");
		return parameter*slope;
	}
}