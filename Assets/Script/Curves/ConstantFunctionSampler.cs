﻿using System.Collections.Generic;
using UnityEngine;

public class ConstantFunctionSampler : AbstractFunctionSampler
{
	public override float GetValue( float parameter )
	{
		if( parameter < domainMin || parameter > domainMax )
			throw new System.Exception("ConstantFunctionSampler::GetValue() - Given parameter is out of the function domain.");
		return 0.0f;
	}
}