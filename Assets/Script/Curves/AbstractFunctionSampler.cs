﻿using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractFunctionSampler : MonoBehaviour
{
	public float domainMin;
	public float domainMax;
	public Color displayColor;
	public uint displaySampleCount;
	LineRenderer lineRenderer;
	
	public AbstractFunctionSampler()
	{
		domainMin = 0.0f;
		domainMax = 1.0f;
		displaySampleCount = 1;
	}

	public abstract float GetValue( float parameter );

	public List<Vector2> GetSamplePointList()
	{
		float domainRange = domainMax - domainMin;
		float sampleStep = domainRange/displaySampleCount;

		List<Vector2> samplePointList = new List<Vector2>();
		for(int i=0;i<=displaySampleCount;i++)
		{
			samplePointList.Add( new Vector2( i*sampleStep + domainMin, GetValue( i*sampleStep + domainMin ) ) );
		}

		return samplePointList;
	}

	private void OnDrawGizmos()
	{
		Gizmos.color = displayColor;
		List<Vector2> samplePointList = GetSamplePointList();
		for(int i=0; i<samplePointList.Count-1; i++)
		{
			Gizmos.DrawLine(transform.position + new Vector3(samplePointList[i].x, samplePointList[i].y, transform.position.z),
							transform.position + new Vector3(samplePointList[i+1].x, samplePointList[i+1].y, transform.position.z));
		}
	}

	private void OnValidate()
	{
		if( domainMax - domainMin < 0 )
		{
			Debug.LogWarning("AbstractFunctionSample::OnValidate() - Invalid domain range input.");
		}
	}

	
}