﻿using UnityEngine;

public class Triangle
{
    public static float[] GetAngles(Vector2 point0, Vector2 point1, Vector2 point2)
    {
        float side0 = (point1 - point2).magnitude;
        float side1 = (point2 - point0).magnitude;
        float side2 = (point0 - point1).magnitude;

        float[] angles = new float[3];
        angles[0] = Mathf.Acos((side1 * side1 + side2 * side2 - side0 * side0) / (2 * side1 * side2));
        angles[1] = Mathf.Acos((side2 * side2 + side0 * side0 - side1 * side1) / (2 * side2 * side0));
        angles[2] = Mathf.PI - angles[0] - angles[1];

        return angles;
    }
}
