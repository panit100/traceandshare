﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;

public class RewardVideoManager : MonoBehaviour
{
    [SerializeField] VideoPlayer RewardVideo;
    // Start is called before the first frame update
    void Start()
    {
            RewardVideo.Play();
    }
}
