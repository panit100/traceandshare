﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class CheckDrawByCollider : MonoBehaviour
{   
    [SerializeField] string nextScene;
    [SerializeField] int Error = 10;
    [SerializeField] GameObject ClockMain;
    [SerializeField] GameObject ClockBG;
    [SerializeField] SpriteRenderer ClockFade;
    [SerializeField] VideoPlayer StarVideo;
    [SerializeField] GameObject StarPlayer;
    bool Finish = false;
    GameObject[] Line;

    void Start()
    {
        Line = GameObject.FindGameObjectsWithTag("Line");
    }
    void Update()
    {
        if(!Finish){
            Check();
        }
    }
    void Check(){
        
        if(Line.Length <= Error){
            if(StarVideo == null)
            SceneManager.LoadScene(nextScene);
            else
            {
                ifIsAllPanited();
                Finish = true;
            }
        }
    }

    
    void ifIsAllPanited(){
        ClockMain.GetComponent<SpriteMask>().enabled = false;
        ClockBG.SetActive(true);
        ClockFade.gameObject.SetActive(true);
        StarPlayer.SetActive(true);
        
        StarVideo.Play();
        StarVideo.loopPointReached += isVideoEnd;          
    }

    void isVideoEnd(VideoPlayer vp){
        SceneManager.LoadScene(nextScene);
    }
}
