﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadAsyncDrawingStage : MonoBehaviour
{
    [SerializeField] string loadscene;
    [SerializeField] string[] UnloadScene;
    AsyncOperation asyncOperation;

    
    void Start()
    {
        StartCoroutine(LoadScene());
    }


    IEnumerator LoadScene()
    {
        yield return null;

        asyncOperation = SceneManager.LoadSceneAsync(loadscene);
        asyncOperation.allowSceneActivation = false;
        while (!asyncOperation.isDone)
        {
            yield return null;
        }
    }

    public void UnloadSceneActive(){
        foreach(string n in UnloadScene){
            SceneManager.UnloadSceneAsync(n);
        }      
        AllowSceneActivate();    
    }

    void AllowSceneActivate(){
        asyncOperation.allowSceneActivation = true; 
    }
}
