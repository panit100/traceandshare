﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawByCollider : MonoBehaviour
{
    void DrawWithMouse(){
            Vector2 MousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            
            if(Input.GetMouseButton(0)){    

                if(GetComponent<Collider2D>() == Physics2D.OverlapPoint(MousePos)){
                    Destroy(gameObject);
                }
            }
    }

    void DrawWithTouch(){
        if(Input.touchCount > 0){
            Touch touch = Input.GetTouch(0);
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(touch.position);

            switch(touch.phase){
                
                case TouchPhase.Moved:
                    if(GetComponent<Collider2D>() == Physics2D.OverlapPoint(touchPos)){
                        Destroy(gameObject);
                }
                    break;
            }
        }
    }

    

    void Update(){
        DrawWithMouse();
        //DrawWithTouch();
    }
    
}
